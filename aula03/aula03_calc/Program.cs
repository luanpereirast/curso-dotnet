﻿using aula03_calc;

internal class Program
{
    static void Main(string[] args)
    {
        var calc = new CalculadoraSimples();
        Console.WriteLine("Adicao= "+ calc.Adicao(1, 2));
        Console.WriteLine("Subtracao= "+ calc.Subtracao(8, 2));
        Console.WriteLine("Multiplicacao= "+ calc.Multiplicacao(2, 3));
        Console.WriteLine("Divisao= "+ calc.Divisao(4, 2).ToString());
    }
}