﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aula03_calc
{
    internal interface ICalculadora
    {
        public int Adicao(int a, int b);
        public int Subtracao(int a, int b);
        public int Multiplicacao(int a, int b);
        public int Divisao(int a, int b);
    }
}
