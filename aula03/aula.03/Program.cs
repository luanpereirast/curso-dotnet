﻿using System.Reflection;

namespace aula03
{
    internal class Program {
        static void Main(string[] args)
        {
            var cachorro = new Animal(new DateTime(), "Cachorro");
            Console.WriteLine(cachorro.ToString());
            cachorro.EmitirSom();
            cachorro.Andar();

            var dog = new Cachorro(new DateTime());
            dog.Latir();
            dog.Andar();
            dog.EmitirSom();
        }
    }
}