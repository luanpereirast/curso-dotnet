﻿public class Animal : IAnimal
{
    private DateTime dateTime;

    public DateTime DataNascimento { get; private set; }
	public int Idade { get; set; }
	public string Especie { get; set; } = String.Empty;
	public virtual void EmitirSom()
	{
		Console.WriteLine("Emitindo som...");
	}

	public Animal(DateTime dataNascimento, string especie) 
	{
		this.DataNascimento = dataNascimento;
        this.Especie = especie;
    }

	public string DataNascimentoFormatada() 
	{
		return this.DataNascimento.ToString("MM/yyyy");
	}
	public void Andar()
	{
		Console.WriteLine("Andando...");
	}
}
