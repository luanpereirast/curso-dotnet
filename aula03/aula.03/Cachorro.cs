﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aula03
{
    class Cachorro : Animal
    {
        public Cachorro(DateTime dataNascimento) : base(dataNascimento, "Cachorro") { }
        public void Latir() { Console.WriteLine("Au Au!"); }
    }
}
