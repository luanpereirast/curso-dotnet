﻿public class CartaUno : Carta<Uno>
{
    public string Cor { get; set; }
    public CartaUno(Uno uno, string tipo, string cor) : base(uno, tipo)
    {
        Cor = cor;
    }
    public override string ToString()
    {
        return $"{Valor} {Cor} de {Tipo}";
    }
}
