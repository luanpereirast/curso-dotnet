﻿using System.Drawing;

public class Carta<T> where T : ITipo
{
    public T Tipo { get; set; }
    public string Valor { get; set; }

    public Carta(T tipo, string valor)
    {
        Tipo = tipo;
        Valor = valor;
    }

    public override string ToString()
    {
        return $"{Valor} de {Tipo}";
    }
}

