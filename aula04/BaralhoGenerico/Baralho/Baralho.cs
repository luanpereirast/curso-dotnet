﻿public abstract class Baralho<T> where T : ITipo
{
    public List<Carta<T>> Cartas { get; set; }
    public T Tipo { get; set; }

    public Baralho(T tipo)
    {
        Tipo = tipo;
        Cartas = new List<Carta<T>>();
    }
    public void Embaralhar()
    {
        var random = new Random();
        for (int i = 0; i < Cartas.Count; i++)
        {
            var j = random.Next(Cartas.Count);
            var temp = Cartas[i];
            Cartas[i] = Cartas[j];
            Cartas[j] = temp;
        }
    }
}

