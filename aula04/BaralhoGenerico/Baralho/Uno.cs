﻿public class Uno : ITipo
{
    public string Nome { get; set; }

    public Uno()
    {
        Nome = "Uno";
    }
    public override string ToString()
    {
        return Nome;
    }
}

