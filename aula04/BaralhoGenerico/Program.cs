﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");
//Jogo de Cartas Genérico:

//Projete classes abstratas para representar um jogo de cartas, como Carta e Baralho.
//Utilize generics para permitir a criação de diferentes tipos de cartas e baralhos (por exemplo, cartas de baralho francês ou baralho de poker).

//var carta = new Carta<String>();
var uno = new Uno();
var carta1 = new CartaUno(uno, "1", "vermelho");
var carta2 = new CartaUno(uno, "2", "vermelho");
var carta3 = new CartaUno(uno, "3", "vermelho");

var baralho = new BaralhoUno(uno);

baralho.Cartas.Add(new CartaUno(uno, "1", "vermelho"));
baralho.Cartas.Add(new CartaUno(uno, "2", "vermelho"));
baralho.Cartas.Add(new CartaUno(uno, "3", "vermelho"));
baralho.Cartas.Add(new CartaUno(uno, "1", "azul"));
baralho.Cartas.Add(new CartaUno(uno, "2", "azul"));
baralho.Cartas.Add(new CartaUno(uno, "3", "azul"));
baralho.Cartas.Add(new CartaUno(uno, "1", "verde"));
baralho.Cartas.Add(new CartaUno(uno, "2", "verde"));
baralho.Cartas.Add(new CartaUno(uno, "3", "verde"));
baralho.Cartas.Add(new CartaUno(uno, "1", "amarelo"));
baralho.Cartas.Add(new CartaUno(uno, "2", "amarelo"));
baralho.Cartas.Add(new CartaUno(uno, "3", "amarelo"));

baralho.Embaralhar();

foreach (var carta in baralho.Cartas)
{
    Console.WriteLine(carta);
}