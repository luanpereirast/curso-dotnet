﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormasGeometricas.Formas
{
    public class Triangulo : FormaGeometrica
    {
        decimal _ladoA;
        decimal _ladoB;
        decimal _ladoC;
        Triangulo(decimal ladoA, decimal ladoB, decimal ladoC)
        {
            _ladoA = ladoA;
            _ladoB = ladoB;
            _ladoC = ladoC;
        }

        public decimal SomaLados()
        {
            return _ladoA + _ladoB + _ladoC;
        }

        public override decimal CalcularArea() 
        {
            return srqt(pValue() * (pValue()-_ladoA) * (pValue()-_ladoB) * (pValue()-_ladoC))
        }

        public override decimal CalcularPerimetro()
        {
            return (_base + _altura) + ;
        }
        private decimal pValue()
        {
            return SomaLados() / 2;
        }
    }
}
